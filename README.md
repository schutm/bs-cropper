bs-cropper
==========

Synopsis
--------
bs-cropper contains bindings to [cropperjs](https://github.com/fengyuanchen/cropperjs).



Installation
------------

```
yarn add bs-cropper
```

And don't forget to add it to your bsconfig.json:

```
  "bs-dependencies": [
    "bs-cropper",
    ...
  ],

```


Usage
-----

```
let cropper = Cropper.make "#crop" in
...
```


Bug tracker
-----------
[Open a new issue](https://gitlab.com/schutm/bs-cropper/issues) for bugs
or feature requests. Please search for existing issues first.

Bugs or feature request will be fixed in the following order, if time
permits:

1. It has a pull-request with a working and tested fix.
2. It is easy to fix and has benefit to myself or a broader audience.
3. It puzzles me and triggers my curiosity to find a way to fix.


Acknowledgements
----------------
Of course this library would be impossible without the excellent
[cropperjs](https://github.com/fengyuanchen/cropperjs) library.


Contributing
------------
Anyone and everyone is welcome to [contribute](CONTRIBUTING.md).


License
-------
This software is licensed under the [ISC License](LICENSE).

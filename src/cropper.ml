module Web = struct
  type node

  type document = <
    getElementById : string -> node Js.null_undefined [@bs.meth];
  > Js.t

  type htmlCanvasElement = <
    toDataURL : string -> float -> string [@bs.meth]
  > Js.t

  external document : document = "document" [@@bs.val]

  let getElementById id = document##getElementById id

  let toDataURL canvas type' quality = canvas##toDataURL type' quality
end

type t

type drag_mode =
  | Crop
  | Move
  | None

type view_mode =
  | NoRestrictions
  | NoExceeding
  | Fit
  | Fill

type image_smoothing_quality =
  | None
  | Low
  | Medium
  | High

external cropper_constructor :
     Web.node Js.nullable
  -> < aspectRatio : float
     ; autoCrop : bool
     ; autoCropArea : float
     ; background : bool
     ; center : bool
     ; checkCrossOrigin : bool
     ; checkOrientation : bool
     ; cropBoxMovable : bool
     ; cropBoxResizable : bool
     (* ; data : object *)
     ; dragMode : string
     ; initialAspectRatio : float
     ; guides : bool
     ; highlight : bool
     ; minCanvasHeight : float
     ; minCanvasWidth : float
     ; minCropBoxHeight : float
     ; minCropBoxWidth : float
     ; minContainerHeight : float
     ; minContainerWidth : float
     ; modal : bool
     ; movable : bool
     ; preview : string
     ; rotatable : bool
     ; responsive : bool
     ; restore : bool
     ; scalable : bool
     ; toggleDragModeOnDblclick : bool
     ; viewMode : int
     ; wheelZoomRatio : float
     ; zoomable : bool
     ; zoomOnTouch : bool
     ; zoomOnWheel : bool
     (*; ready: callback function *)
     (*; cropstart: callback function *)
     (*; cropmove: callback function *)
     (*; cropend: callback function *)
     (*; crop: callback function *)
     (*; zoom: callback function *) >
     Js.t
  -> t = "default"
  [@@bs.new] [@@bs.module "cropperjs"]

external disable : t -> t = "disable" [@@bs.send]

external enable : t -> t = "enable" [@@bs.send]

external replace : t -> string -> t = "replace" [@@bs.send]

external cropper_get_cropped_canvas :
     t
  -> < width : float
     ; height : float
     ; minWidth : float
     ; minHeight : float
     ; maxWidth : float
     ; maxHeight : float
     ; fillColor : string
     ; imageSmoothingEnabled : bool
     ; imageSmoothingQuality : string >
     Js.t
  -> Web.htmlCanvasElement = "getCroppedCanvas"
  [@@bs.send]

let make
    ?(aspect_ratio = Js.Float._NaN)
    ?(auto_crop = true)
    ?(auto_crop_area = 0.8)
    ?(background = true)
    ?(center = true)
    ?(check_cross_origin = true)
    ?(check_orientation = true)
    ?(crop_box_movable = true)
    ?(crop_box_resizable = true)
    ?(drag_mode = Crop)
    ?(guides = true)
    ?(highlight = true)
    ?(initial_aspect_ratio = Js.Float._NaN)
    ?(modal = true)
    ?(min_canvas_height = 0.0)
    ?(min_canvas_width = 0.0)
    ?(min_container_height = 100.0)
    ?(min_container_width = 200.0)
    ?(min_crop_box_width = 0.0)
    ?(min_crop_box_height = 0.0)
    ?(movable = true)
    ?(preview = "")
    ?(responsive = true)
    ?(restore = true)
    ?(rotatable = true)
    ?(scalable = true)
    ?(toggle_drag_mode_on_dblclick = true)
    ?(view_mode = NoRestrictions)
    ?(wheel_zoom_ratio = 0.1)
    ?(zoomable = true)
    ?(zoom_on_touch = true)
    ?(zoom_on_wheel = true)
    id =
  let drag_mode =
    match drag_mode with
    | Crop -> "crop"
    | Move -> "move"
    | None -> "none"
  in
  let view_mode =
    match view_mode with
    | NoRestrictions -> 0
    | NoExceeding -> 1
    | Fit -> 2
    | Fill -> 3
  in
  cropper_constructor
    (Web.getElementById id)
    [%bs.obj
      { aspectRatio = aspect_ratio
      ; autoCrop = auto_crop
      ; autoCropArea = auto_crop_area
      ; background
      ; center
      ; checkCrossOrigin = check_cross_origin
      ; checkOrientation = check_orientation
      ; cropBoxMovable = crop_box_movable
      ; cropBoxResizable = crop_box_resizable
      ; dragMode = drag_mode
      ; initialAspectRatio = initial_aspect_ratio
      ; guides
      ; highlight
      ; minCanvasHeight = min_canvas_height
      ; minCanvasWidth = min_canvas_width
      ; minContainerHeight = min_container_height
      ; minContainerWidth = min_container_width
      ; minCropBoxHeight = min_crop_box_height
      ; minCropBoxWidth = min_crop_box_width
      ; modal
      ; movable
      ; preview
      ; responsive
      ; restore
      ; rotatable
      ; scalable
      ; toggleDragModeOnDblclick = toggle_drag_mode_on_dblclick
      ; viewMode = view_mode
      ; wheelZoomRatio = wheel_zoom_ratio
      ; zoomable
      ; zoomOnTouch = zoom_on_touch
      ; zoomOnWheel = zoom_on_wheel
      }]


let get_cropped_image_as_data_uri
    ?(min_width = 0.0)
    ?(min_height = 0.0)
    ?(max_width = infinity)
    ?(max_height = infinity)
    ?(fill_color = "transparent")
    ?(image_smoothing = Low)
    ?(format = "png")
    width
    height
    cropper =
  let (image_smoothing_enabled, image_smoothing_quality) =
    match image_smoothing with
    | None -> (false, "low")
    | Low -> (true, "low")
    | Medium -> (true, "medium")
    | High -> (true, "high")
  in
  let c =
  cropper_get_cropped_canvas
    cropper
    [%bs.obj
      { width
      ; height
      ; minWidth = min_width
      ; minHeight = min_height
      ; maxWidth = max_width
      ; maxHeight = max_height
      ; fillColor = fill_color
      ; imageSmoothingEnabled = image_smoothing_enabled
      ; imageSmoothingQuality = image_smoothing_quality
      }]
  in Web.toDataURL c ("image/" ^ format) 0.92
